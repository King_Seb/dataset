CREATE TABLE dataset3(
	id SERIAL PRIMARY KEY,
	score INTEGER,
	Q1 INTEGER,
	Q2 INTEGER,
	Q3 INTEGER,
	Q4 INTEGER,
	Q5 INTEGER,
	Q6 INTEGER,
	Q7 INTEGER,
	Q8 INTEGER,
	Q9 INTEGER,
	Q10 INTEGER,
	Q11 INTEGER,
	Q12 INTEGER,
	Q13 INTEGER,
	Q14 INTEGER,
	Q15 INTEGER,
	Q16 INTEGER,
	Q17 INTEGER,
	Q18 INTEGER,
	Q19 INTEGER,
	Q20 INTEGER,
	Q21 INTEGER,
	Q22 INTEGER,
	Q23 INTEGER,
	Q24 INTEGER,
	Q25 INTEGER,
	Q26 INTEGER,
	Q27 INTEGER,
	Q28 INTEGER,
	Q29 INTEGER,
	Q30 INTEGER,
	Q31 INTEGER,
	Q32 INTEGER,
	Q33 INTEGER,
	Q34 INTEGER,
	Q35 INTEGER,
	Q36 INTEGER,
	Q37 INTEGER,
	Q38 INTEGER,
	Q39 INTEGER,
	Q40 INTEGER,
	seconds_elapsed INTEGER,
	gender INTEGER,
	age INTEGER
);
