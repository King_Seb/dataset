-- ------------------------------------------------------------------
-- Compares the total of males and females for dataset1 and dataset 2
-- ------------------------------------------------------------------

SELECT
	count(dataset1.gender) set1_gender_total,
	count(dataset2.gender) set2_gender_total,
	count(dataset1.gender) FILTER (WHERE dataset1.gender = 1) AS set1_males,
	count(dataset2.gender) FILTER (WHERE dataset2.gender = 1) AS set2_males,
	count(dataset1.gender) FILTER (WHERE dataset1.gender = 2) AS set1_females,
	count(dataset2.gender) FILTER (WHERE dataset2.gender = 2) AS set2_females
FROM dataset1 INNER JOIN dataset2 ON dataset1.id = dataset2.id;
